import { MySampleCalculatorPage } from './app.po';

describe('my-sample-calculator App', () => {
  let page: MySampleCalculatorPage;

  beforeEach(() => {
    page = new MySampleCalculatorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
