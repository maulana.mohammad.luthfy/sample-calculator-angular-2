import { Component } from '@angular/core';
import { Operation } from './operation';
import { OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'My Simple Calculator';
  operation: Operation = new Operation();
  operations: Operation[] = [];
  total: any;

  onSubmit(): void {
    if (!this.operation.operator) {
      window.alert('Please select operator!');
      return;
    }
    this.operations.push(this.operation);
    this.operation = new Operation();
    this.onChange();
  }

  onRemove(index: number): void {
    const filter = this.operations.filter((o, idx) => idx !== index);
    this.operations = filter;
  }

  ngOnInit() {
    this.onChange();
  }

  onChange(): void {
    this.total = this.operation.total();
  }
}
