export class Operation {
  input1: number;
  input2: number;
  operator: string;

  constructor() {
    this.input1 = 0;
    this.input2 = 0;
  }

  total() {
    switch (this.operator) {
      case '+':
        return this.input1 + this.input2;
      case '-':
        return this.input1 - this.input2;
      case 'x':
        return this.input1 * this.input2;
      case ':':
        return this.input1 / this.input2;
      default:
        return 'invalid';
    }
  }
}
