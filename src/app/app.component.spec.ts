import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'My Simple Calculator'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('My Simple Calculator');
  }));

  it(`should return 4 when 1 + 3'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    app.operation.input1 = 1;
    app.operation.input2 = 3;
    app.operation.operator = '+';
    expect(app.operation.total()).toEqual(4);
  }));

  it(`should return 1 when 3 / 3'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    app.operation.input1 = 3;
    app.operation.input2 = 3;
    app.operation.operator = ':';
    expect(app.operation.total()).toEqual(1);
  }));

  it(`should return -6 when -3 - 3'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    app.operation.input1 = -3;
    app.operation.input2 = 3;
    app.operation.operator = '-';
    expect(app.operation.total()).toEqual(-6);
  }));

  it(`should return 120 when 15 * 8'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    app.operation.input1 = 15;
    app.operation.input2 = 8;
    app.operation.operator = 'x';
    expect(app.operation.total()).toEqual(120);
  }));

  it(`should return invalid when no operator`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    app.operation.input1 = -3;
    app.operation.input2 = 3;
    app.operation.operator = undefined;
    expect(app.operation.total()).toEqual('invalid');
  }));

  // it('should render title in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('app works!');
  // }));
});
